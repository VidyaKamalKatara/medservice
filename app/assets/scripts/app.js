import $ from 'jquery';
import "lazysizes";
import "../styles/styles.css";
import MobileMenu from "./modules/MobileMenu";
import RevealOnScroll from "./modules/RevealOnScroll";
import SmoothScroll from "./modules/SmoothScroll";
import ActiveLinks from  "./modules/ActiveLinks";
import Modal from  "./modules/Modal";


// handles Mobile Menu/Header
let mobileMenu = new MobileMenu();

//handles to revealon scroll
new RevealOnScroll($("#our-beginning"));
new RevealOnScroll($("#departments"));
new RevealOnScroll($("#counters"));
new RevealOnScroll($("#testimonials"));

//alert("Hello hii");
//console.log("hii");

//Adding smooth scroll functionality to our header links
new SmoothScroll();

new ActiveLinks();
new Modal();

if(module.hot){
	module.hot.accept();
}