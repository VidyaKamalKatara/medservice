import $ from 'jquery';
class Modal{
	constructor(){
		this.openModalButton =$(".open-modal");
		this.modal = $(".modal");
		this.closeModalButton =$(".modal-close");
		this.events();
	}
	events(){
		this.openModalButton.click(this.openModal.bind(this));
		this.closeModalButton.click(this.closeModal.bind(this));
		
		$(document).keyup(this.keyPressHandler.bind(this));
		
	}
	openModal(){
		this.modal.addClass("modal-is-visible");
		return false;// this will nt perform its original working , sam elike prevent default 
	}
	closeModal(){
		this.modal.removeClass("modal-is-visible");
	}
	keyPressHandler(e){
		if(e.keyCode == 27){
			this.modal.closeModal();
		}
	}
}

export default Modal;